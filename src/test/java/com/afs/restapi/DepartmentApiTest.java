package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.DepartmentRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.converter.json.Jackson2ObjectMapperBuilder.json;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DepartmentApiTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        departmentRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    @Test
    void should_add_employee_to_department() throws Exception {
        Department department = new Department("department1");
        Department savedDepartment = departmentRepository.save(department);
        Employee employee = new Employee(1L, "Susan", 20, "Female", 100);
        Employee savedEmployee = employeeRepository.save(employee);

        mockMvc.perform(post("/departments/{departmentId}/add", savedDepartment.getId())
                    .param("employeeId", savedEmployee.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(savedDepartment.getId()))
                .andExpect(jsonPath("$.name").value(department.getName()))
                .andExpect(jsonPath("$.employees.length()").value(1))
                .andExpect(jsonPath("$.employees[0].id").value(savedEmployee.getId()))
                .andExpect(jsonPath("$.employees[0].name").value(employee.getName()))
                .andExpect(jsonPath("$.employees[0].age").value(employee.getAge()))
                .andExpect(jsonPath("$.employees[0].gender").value(employee.getGender()))
                .andExpect(jsonPath("$.employees[0].salary").value(employee.getSalary()));
    }

    @Test
    void should_remove_employee_from_department() throws Exception {
        Department department = new Department("department1");
        Department savedDepartment = departmentRepository.save(department);
        Employee employee = new Employee(1L, "Susan", 20, "Female", 100);
        employee.setDepartmentId(savedDepartment.getId());
        Employee savedEmployee = employeeRepository.save(employee);

        mockMvc.perform(post("/departments/{departmentId}/remove", savedDepartment.getId())
                    .param("employeeId", savedEmployee.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(savedDepartment.getId()))
                .andExpect(jsonPath("$.name").value(department.getName()))
                .andExpect(jsonPath("$.employees.length()").value(0));

        assertNull(employeeRepository.findById(savedEmployee.getId()).get().getDepartmentId());
    }

    @Test
    void should_return_employee_from_department() throws Exception {
        Department department = new Department("department1");
        Department savedDepartment = departmentRepository.save(department);
        Employee employee1 = new Employee(null, "Susan", 20, "Female", 100);
        Employee employee2 = new Employee(null, "Joyce", 20, "Female", 100);
        Employee employee3 = new Employee(null, "Handsome", 20, "Male", 100);
        employee1.setDepartmentId(savedDepartment.getId());
        employee3.setDepartmentId(savedDepartment.getId());
        Employee savedEmployee1 = employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        Employee savedEmployee3 = employeeRepository.save(employee3);

        mockMvc.perform(get("/departments/{departmentId}/employees", savedDepartment.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(result -> Objects.equals(
                        result.getResponse().getContentAsString(),
                        objectMapper.writeValueAsString(List.of(savedEmployee1, savedEmployee3)))
                );
    }

    @Test
    void should_return_company_from_department() throws Exception {
        Company company = new Company(null, "xyz");
        Company savedCompany = companyRepository.save(company);
        Department department = new Department("department");
        department.setCompanyId(savedCompany.getId());
        Department savedDepartment = departmentRepository.save(department);

        mockMvc.perform(get("/departments/{departmentId}/company", savedDepartment.getId()))
                .andExpect(status().isOk())
                .andExpect(result -> Objects.equals(
                        result.getResponse().getContentAsString(),
                        objectMapper.writeValueAsString(savedCompany)
                ));
    }
}
