package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.DepartmentService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping(path = "/{departmentId}/add", params = {"employeeId"})
    public Department addEmployee(@PathVariable Long departmentId, @RequestParam Long employeeId) {
        return departmentService.addEmployee(departmentId, employeeId);
    }

    @PostMapping(path = "/{departmentId}/remove", params = {"employeeId"})
    public Department removeEmployee(@PathVariable Long departmentId, @RequestParam Long employeeId) {
        return departmentService.removeEmployee(departmentId, employeeId);
    }

    @GetMapping("/{departmentId}/employees")
    public List<Employee> getEmployees(@PathVariable Long departmentId) {
        return departmentService.findAllEmployees(departmentId);
    }

    @GetMapping("/{departmentId}/company")
    public Company getCompany(@PathVariable Long departmentId) {
        return departmentService.findCompany(departmentId);
    }
}
