package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Department;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.DepartmentNotFoundException;
import com.afs.restapi.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompanyService companyService;

    private Department findById(Long id) {
        return departmentRepository.findById(id)
                .orElseThrow(DepartmentNotFoundException::new);
    }

    public Department addEmployee(Long departmentId, Long employeeId) {
        employeeService.setEmployeeToDepartment(employeeId, departmentId);
        return findById(departmentId);
    }

    public Department removeEmployee(Long departmentId, Long employeeId) {
        employeeService.removeEmployeeFromDepartment(employeeId, departmentId);
        return findById(departmentId);
    }

    public List<Employee> findAllEmployees(Long departmentId) {
        return findById(departmentId)
                .getEmployees();
    }

    public Company findCompany(Long departmentId) {
        Long companyId = findById(departmentId)
                .getCompanyId();
        return companyService.findById(companyId);
    }
}
